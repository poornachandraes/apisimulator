# README #

Spring application to simulate API response for endpoints

### Includes ###

* Supports only GET response for any endpoint
* Test Cases
* Application context : http://localhost:8080/apisimulator/

### Prerequisites ###

* Java 8
* Maven

### Information ###

* Returns generic response to any API call
* `/tickets` and `/reports` API gives specific response

### How to run application ###

* Build `mvn clean install`
* Run `mvn spring-boot:run`
* Run with profile `mvn install spring-boot:run -Dspring-boot.run.profiles=dev`