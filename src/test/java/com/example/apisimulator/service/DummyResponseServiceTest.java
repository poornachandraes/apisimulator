package com.example.apisimulator.service;

import com.example.apisimulator.model.Report;
import com.example.apisimulator.model.SimpleRes;
import com.example.apisimulator.model.Ticket;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DummyResponseServiceTest {

    @Autowired
    private List<DummyResponseHandler> responseHandlers;

    @Autowired
    private GenericDummyResponseHandler genericHandler;

    private DummyResponseService service;

    @Before
    public void setUp() {
        service = new DummyResponseService(responseHandlers, genericHandler);
    }

    @Test
    public void should_get_standard_response_with_description() {
        String uri = "/tickets";
        Map<String, String []> parameters = new HashMap<>();
        parameters.put("description", new String[]{"some value"});
        Ticket actualRes = (Ticket) service.getResponse(uri, parameters);

        assertEquals("some value", actualRes.getDescription());
    }

    @Test
    public void should_get_standard_response_with_empty_description() {
        String uri = "/tickets";
        Map<String, String []> parameters = new HashMap<>();
        Ticket actualRes = (Ticket) service.getResponse(uri, parameters);

        assertEquals("", actualRes.getDescription());
    }

    @Test
    public void should_get_standard_response_for_additional_tickets_uri() {
        String uri = "/tickets/23";
        Map<String, String []> parameters = new HashMap<>();
        Ticket actualRes = (Ticket) service.getResponse(uri, parameters);

        assertEquals("", actualRes.getDescription());
    }

    @Test
    public void should_get_standard_response_for_reports_uri() {
        String uri = "/reports";
        Map<String, String []> parameters = new HashMap<>();
        Report actualRes = (Report) service.getResponse(uri, parameters);

        assertNotNull(actualRes.getTotalTickets());
    }

    @Test
    public void should_get_standard_response_for_additional_reports_uri() {
        String uri = "/reports/12";
        Map<String, String []> parameters = new HashMap<>();
        Report actualRes = (Report) service.getResponse(uri, parameters);

        assertNotNull(actualRes.getTotalTickets());
    }

    @Test
    public void should_get_standard_response_for_no_handler_uri() {
        String uri = "/logins";
        Map<String, String []> parameters = new HashMap<>();
        SimpleRes actualRes = service.getResponse(uri, parameters);

        assertEquals("/logins", actualRes.getUri());
    }

    @Test
    public void should_get_standard_response_for_any_get_uri() {
        String uri = "/forgotPassword/user12";
        Map<String, String []> parameters = new HashMap<>();
        SimpleRes actualRes = service.getResponse(uri, parameters);

        assertEquals("/forgotPassword/user12", actualRes.getUri());
    }
}
