package com.example.apisimulator.controller;

import com.example.apisimulator.model.SimpleRes;
import com.example.apisimulator.service.ResponseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
public class RootController {

    @Autowired
    ResponseService responseService;

    Logger logger = LoggerFactory.getLogger(RootController.class);

    @GetMapping("/**")
    public SimpleRes processAllRequests(HttpServletRequest request) {
        String contextPath = request.getContextPath();
        String uri = request.getRequestURI().substring(contextPath.length());
        Map<String, String[]> parameters = request.getParameterMap();
        logger.info("Request Received : " + uri);
        return responseService.getResponse(uri, parameters);
    }
}
