package com.example.apisimulator.model;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class SimpleRes {

    String uri;
    LocalDateTime requestedAt;

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public LocalDateTime getRequestedAt() {
        return requestedAt;
    }

    public void setRequestedAt(LocalDateTime requestedAt) {
        this.requestedAt = requestedAt;
    }
}
