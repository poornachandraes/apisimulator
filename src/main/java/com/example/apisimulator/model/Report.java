package com.example.apisimulator.model;

public class Report extends SimpleRes {

    private Integer totalTickets;
    private Integer openTickets;

    public Integer getTotalTickets() {
        return totalTickets;
    }

    public void setTotalTickets(Integer totalTickets) {
        this.totalTickets = totalTickets;
    }

    public Integer getOpenTickets() {
        return openTickets;
    }

    public void setOpenTickets(Integer openTickets) {
        this.openTickets = openTickets;
    }
}
