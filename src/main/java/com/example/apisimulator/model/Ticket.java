package com.example.apisimulator.model;

public class Ticket extends SimpleRes {

    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
