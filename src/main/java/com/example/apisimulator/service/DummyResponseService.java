package com.example.apisimulator.service;

import com.example.apisimulator.model.SimpleRes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class DummyResponseService implements ResponseService {

    private List<DummyResponseHandler> responseHandlers;
    private GenericDummyResponseHandler genericHandler;

    @Autowired
    public DummyResponseService(List<DummyResponseHandler> responseHandlers, GenericDummyResponseHandler genericHandler) {
        this.responseHandlers = responseHandlers;
        this.genericHandler = genericHandler;
    }

    @Override
    public SimpleRes getResponse(String uri, Map<String, String[]> parameters) {

        DummyResponseHandler handler = responseHandlers.stream().filter(h -> h.checkUri(uri)).findFirst().orElse(genericHandler);
        return handler.getResponse(uri, parameters);
    }
}
