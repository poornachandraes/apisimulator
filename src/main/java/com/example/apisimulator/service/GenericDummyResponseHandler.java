package com.example.apisimulator.service;

import com.example.apisimulator.model.SimpleRes;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Map;

@Service
public class GenericDummyResponseHandler implements DummyResponseHandler {

    @Override
    public boolean checkUri(String uri) {
        return false;
    }

    @Override
    public SimpleRes getResponse(String uri, Map<String, String[]> parameters) {
        SimpleRes res = new SimpleRes();
        res.setUri(uri);
        res.setRequestedAt(LocalDateTime.now());
        return res;
    }

}
