package com.example.apisimulator.service;

import com.example.apisimulator.model.SimpleRes;
import com.example.apisimulator.model.Ticket;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Map;

@Service
public class TicketDummyResponseHandler implements DummyResponseHandler {

    private static final String ticketUri = "/tickets";

    public boolean checkUri(String uri) {
        return uri.startsWith(ticketUri);
    }

    @Override
    public SimpleRes getResponse(String uri, Map<String, String[]> parameters) {
        String[] arrayStr = parameters.get("description");
        String description = arrayStr != null ? arrayStr[0] : "";
        Ticket ticket = new Ticket();
        ticket.setUri(uri);
        ticket.setDescription(description.toString());
        ticket.setRequestedAt(LocalDateTime.now());
        return ticket;
    }
}
