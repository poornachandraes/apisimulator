package com.example.apisimulator.service;

import com.example.apisimulator.model.SimpleRes;

import java.util.Map;

public interface ResponseService {

    public SimpleRes getResponse(String uri, Map<String, String[]> parameters);

}
