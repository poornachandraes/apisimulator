package com.example.apisimulator.service;

import com.example.apisimulator.model.Report;
import com.example.apisimulator.model.SimpleRes;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.Random;

@Service
public class ReportDummyResponseHandler implements DummyResponseHandler {

    private static final String reportUri = "/reports";

    @Override
    public boolean checkUri(String uri) {
        return uri.startsWith(reportUri);
    }

    @Override
    public SimpleRes getResponse(String uri, Map<String, String[]> parameters) {
        Random rand = new Random();
        int totalNum = rand.nextInt((100 - 60) + 1) + 60;
        int openNum = rand.nextInt((60 - 10) + 1) + 10;
        Report report = new Report();
        report.setUri(uri);
        report.setRequestedAt(LocalDateTime.now());
        report.setTotalTickets(totalNum);
        report.setOpenTickets(openNum);
        return report;
    }
}
